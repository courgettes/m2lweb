<?php

namespace App\Repository;

use App\Entity\SalleReservable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SalleReservable|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalleReservable|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalleReservable[]    findAll()
 * @method SalleReservable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalleReservableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalleReservable::class);
    }

    // /**
    //  * @return SalleReservable[] Returns an array of SalleReservable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalleReservable
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
