<?php
namespace App\Controller;

use App\Entity\Batiment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class BatimentController extends AbstractController
{
    public function afficherBatiments()
    {
        $batiments =$this->getDoctrine()->getRepository(Batiment::class)->findAll();
        return $this->render('batiment/batiment.html.twig', ['titre'=>"Liste des bureaux",'batiments'=>$batiments]);//'batimements'=>$batiments]);
    }

}