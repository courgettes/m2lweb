<?php
namespace App\Controller;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class CategorieController extends AbstractController
{
     public function afficherCategorie()
     {
         $categories =$this->getDoctrine()->getRepository(Categorie::class)->findAll();
         return $this->render('Categorie.html.twig', ['titre'=>"Lister les categorie",'categories'=>$categories]);
     }

}