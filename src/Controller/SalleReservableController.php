<?php
namespace App\Controller;

use App\Entity\Batiment;
use App\Entity\Etage;
use App\Entity\SalleReservable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SalleReservableController extends AbstractController
{
    public function afficherSalleReservable()
    {
        $salleReservables =$this->getDoctrine()->getRepository(SalleReservable::class)->findAll();
        $etages = $this->getDoctrine()->getRepository(Etage::class)->findAll();
        $batiments = $this->getDoctrine()->getRepository(Batiment::class)->findAll();
        return $this->render('sallereservable/sallereservable.html.twig', ['titre'=>"Liste des salles reservables",'sallereservables'=>$salleReservables,
            'etages'=>$etages, 'batiments'=>$batiments]);
    }
}