<?php
namespace App\Controller;

use App\Entity\Bureau;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class BureauController extends AbstractController
{
    public function afficherBureau()
    {
        $bureaux =$this->getDoctrine()->getRepository(Bureau::class)->findAll();
        return $this->render('bureau/bureau.html.twig', ['titre'=>"Liste des bureaux",'bureaux'=>$bureaux]);//'batimements'=>$batiments]);
    }

    public function afficherBureauDisponible()
    {
        $bureaux =$this->getDoctrine()->getRepository(Bureau::class)->BureauDisponible();
        return$this->render('bureau/BureauDisponible.html.twig',['titre'=>"liste des bureaux disponible",'bureaux'=>$bureaux]);


    }
}