<?php

namespace App\Controller;

use App\Entity\Ligue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ligueController extends AbstractController
{
    public function listerLesLigues(){
        $ligues = $this->getDoctrine()->getRepository(Ligue::class)->findAll();
        return $this->render('ligue/listerLesLigues.html.twig', ['titre' => 'Liste des ligues','ligues'=>$ligues]);
    }
}
