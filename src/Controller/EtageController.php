<?php
namespace App\Controller;

use App\Entity\Etage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class EtageController extends AbstractController
{
    
    public function listEtagesByBatiment() {
        $etages = $this->getDoctrine()->getRepository(Etage::class)->findAll();
        usort($etages, Etage::compareByBatiment());
        $batiments = array();
        $elem = null;
        $etageOfBatiToString = "";
        foreach ($etages as $e) {
            if ($e->getBatiment()->getId() != $elem) {
                if ($elem != null) { // don't add  to the first one
                    $etageOfBatiToString += "</ul>";
                }
                $etageOfBatiToString += "<ul>";
                $etageOfBatiToString += "<h2>".$e->getBatiment()->getId()."</h2>";
                $elem = $e->getBatiment()->getId();
            }
            $etageOfBatiToString += "<li>".$e->getNumero()."</li>";
        }
        $etageOfBatiToString += "</ul>";

        return $this->render('etage/listEtagesByBati.html.twig',['etages'=>$etageOfBatiToString, 'titre'=>"Liste des etages par bâtiment"]);

    }

    public function listEtages() {
        $Etages = $this->getDoctrine()->getRepository(Etage::class)->findAll();
        return $this->render('etage/listEtage.html.twig',['etages'=>$Etages, 'titre'=>"Liste des etages"]);
    }

}