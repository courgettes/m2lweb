<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DefaultController extends AbstractController
{

    public function accueil()
    {
        return $this->render('accueil.html.twig', ['titre'=>"Accueil"]);
    }
    public function afficherInformationsPratiques()
    {
        return $this->render('InformationsPratiques.html.twig',['titre'=>"InformationsPratiques"]);
    }

    public function planm2l()
    {
        return $this->render('planM2l.html.twig', ['titre'=>"Plan de la M2L"]);
    }

}