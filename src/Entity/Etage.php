<?php

namespace App\Entity;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

use Doctrine\ORM\Mapping as ORM;
/**

 * @ORM\Entity(repositoryClass="App\Repository\EtageRepository")

 * @ORM\Table(name="etage")

 */

class Etage
{
        /**

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="etage_idetage_seq")

     * @ORM\Column(type="integer",name="idetage")

     */
    private $id;

    /**

     * @ORM\Column(type="integer")

     */
    private $numero;


    /**
     * @ManyToOne(targetEntity="Batiment")
     * @JoinColumn(name="batiment", referencedColumnName="idbatiment")
     */
    private $batiment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getBatiment(): ?Batiment
    {
        return $this->batiment;
    }

    public function setBatiment(?Batiment $batiment): self
    {
        $this->batiment = $batiment;

        return $this;
    }

    public static function compareByBatiment(Etage $a, Etage $b) {
        $batA = $a->getBatiment()->getId();
        $batB = $b->getBatiment()->getId();

        if ($batA  == $batB) {
            return 0;
        }
        return ($batA < $batB) ? -1 : 1;
    }


}