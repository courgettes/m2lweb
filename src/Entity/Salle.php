<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

use Doctrine\ORM\Mapping as ORM;
/**

 * @ORM\Entity(repositoryClass="App\Repository\SalleRepository")
 * @ORM\Table(name="salle")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="salleType", type="string")
 * @ORM\DiscriminatorMap({"salle"="Salle","bureau"="Bureau","sallereservable"="SalleReservable"})
 */

class Salle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="salle_idsalle_seq")
     * @ORM\Column(type="integer",name="idsalle")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nom;

    /**
     * @ManyToOne(targetEntity="Etage")
     * @JoinColumn(name="situation", referencedColumnName="idetage")
     */
    private $situation;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSituation(): ?Etage
    {
        return $this->situation;
    }

    public function setSituation(int $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

}