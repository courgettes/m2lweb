<?php

namespace App\Entity;

use App\Entity\Categorie;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity(repositoryClass="App\Repository\SalleReservableRepository")
 * @ORM\Table(name="sallereservable")

 */

class SalleReservable extends Salle
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie")
     * @ORM\JoinColumn(name="idcategorie", referencedColumnName="idcategorie")
     */
    private $categorie;

    public function getCategorie(): ?int
    {
        return $this->categorie;
    }

    public function setCategorie(int $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function __toString()
    {
        if($this->categorie == null){
            return $this->getNom();
        }
        else{
            return $this->getNom().$this->categorie->getTypeCategorie;
        }
    }
}