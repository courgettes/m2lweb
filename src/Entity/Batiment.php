<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**

 * @ORM\Entity(repositoryClass="App\Repository\BatimentRepository")

 * @ORM\Table(name="batiment")

 */

class Batiment

{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="batiment_idbatiment_seq")
     * @ORM\Column(type="integer",name="idbatiment")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }



}