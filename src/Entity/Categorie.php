<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**

 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")

 * @ORM\Table(name="categorie")

 */

class Categorie

{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="categorie_idcategorie_seq")
     * @ORM\Column(type="integer",name="idcategorie")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $typecategorie;

    /**
     * @ORM\Column(type="integer")
     */
    private $tariflocation;

    /**
     * @ORM\Column(type="integer")
     */
    private $nblocationgratuites;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypecategorie(): ?string
    {
        return $this->typecategorie;
    }

    public function setTypecategorie(string $typecategorie): self
    {
        $this->typecategorie = $typecategorie;

        return $this;
    }

    public function getTariflocation(): ?int
    {
        return $this->tariflocation;
    }

    public function setTariflocation(int $tariflocation): self
    {
        $this->tariflocation = $tariflocation;

        return $this;
    }

    public function getNblocationgratuites(): ?int
    {
        return $this->nblocationgratuites;
    }

    public function setNblocationgratuites(int $nblocationgratuites): self
    {
        $this->nblocationgratuites = $nblocationgratuites;

        return $this;
    }

    public function __toString()
    {
        return $this->typecategorie;
    }

}