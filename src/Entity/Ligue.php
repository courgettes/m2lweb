<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity(repositoryClass="App\Repository\LigueRepository")

 * @ORM\Table(name="ligue")

 */

class Ligue

{
    /**

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="ligue_idligue_seq")

     * @ORM\Column(type="integer",name="idligue")

     */
    private $id;

    /**

     * @ORM\Column(type="string")

     */
    private $numerorna;


    /**

     * @ORM\Column(type="string")

     */

    private $nom;

    /**

     * @ORM\Column(type="string")

     */
    private $adresse;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumerorna(): ?string
    {
        return $this->numerorna;
    }

    public function setNumerorna(string $numerorna): self
    {
        $this->numerorna = $numerorna;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

}