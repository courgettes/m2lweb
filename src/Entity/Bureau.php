<?php

namespace App\Entity;

use App\Entity\Salle;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**

 * @ORM\Entity(repositoryClass="App\Repository\BureauRepository")
 * @ORM\Table(name="bureau")

 */

class Bureau extends Salle
{
    /**
     * @ORM\Column(type="integer")
     * @ManyToOne(targetEntity="App\Entity\Ligue")
     * @JoinColumn(name="idligue", referencedColumnName="idligue")
     */
    private $occupant;



    public function getOccupant(): ?int
    {
        return $this->occupant;
    }

    public function setOccupant(int $occupant): self
    {
        $this->occupant = $occupant;

        return $this;
    }

    public function estDispo()
    {
        $estdispo = true;
        if ($this->occupant == null) {
            $estdispo = false;
        }
        return $estdispo;
    }

}